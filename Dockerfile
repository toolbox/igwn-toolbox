FROM igwn/base:el9

LABEL com.github.containers.toolbox="true"

# install default toolbox packages
COPY toolbox-packages /
RUN dnf -y install $(<toolbox-packages) && \
    rm /toolbox-packages && \
    dnf clean all

# install extra packages
COPY extra-packages /
RUN dnf -y install $(<extra-packages) && \
    rm /extra-packages && \
    dnf clean all
